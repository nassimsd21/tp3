<?php

namespace App\Controller;
use App\Entity\EcoleDoctorale;
use App\Entity\These;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController//héritage de ma classe HommeController à la classe AbstractController
{
    /**
     * @Route("/home", name="home")//Route: c'est une Annotation Route(Quant un navigateur appelle mon site.com/home cette annotation va appeller la fonction index ci_desous)
     */
    public function index()//cette fonction va traiter la demande) et elle va nous renvoyer le fichier index.html.twig qui se trouve dans le dossier home dans le template
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ecoleDoctoraleRepository= $entityManager->getRepository(EcoleDoctorale::class);
        $ecoleDoctorale= $ecoleDoctoraleRepository->findAll();

        if(empty($ecoleDoctorale)){
            $ecole1 = new  EcoleDoctorale ();
            $ecole1->setNom("UniversitéMontpellier");
            $ecole1->setLien("###############");
            $entityManager->persist($ecole1);

            $these1 = new These();
            $these1->setTitre("Climat");
            $these1->setDescription("réchauffement climatique");
            $these1->setContact("zi hachmi");
            $these1->setEcoleDoctorale($ecole1);
            $entityManager->persist($these1);

            $these2 = new These();
            $these2->setTitre("Climat2");
            $these2->setDescription("réchauffementClimatique2");
            $these2->setContact("zi hachmi");
            $these2->setEcoleDoctorale($ecole1);
            $entityManager->persist($these2);

            $entityManager->flush();

        }
        



        return $this->render('home/index.html.twig', [

           'ecoleDoctorales'=>$ecoleDoctoraleRepository->findAll(), 
            
            
            
        ]);
    }
}
